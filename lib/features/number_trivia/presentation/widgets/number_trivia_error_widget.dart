import 'package:clean_architecture_riverpod/core/helpers/fix_exception_message.dart';
import 'package:flutter/material.dart';

class NumberTriviaErrorWidget extends StatelessWidget {
  const NumberTriviaErrorWidget(this.error, {Key? key}) : super(key: key);

  final Object error;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height / 3,
      child: Center(
        child: SingleChildScrollView(
          child: Text(
            fixExceptionMessage('$error'),
            style: const TextStyle(fontSize: 25),
            textAlign: TextAlign.center,
          ),
        ),
      ),
    );
  }
}
