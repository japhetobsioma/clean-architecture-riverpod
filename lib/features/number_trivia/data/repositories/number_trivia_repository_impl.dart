import 'package:clean_architecture_riverpod/features/number_trivia/data/data_sources/local_data/number_trivia_local_data.dart';
import 'package:clean_architecture_riverpod/features/number_trivia/data/data_sources/remote_data/number_trivia_remote_data.dart';
import 'package:clean_architecture_riverpod/features/number_trivia/domain/entites/number_trivia_entity.dart';
import 'package:clean_architecture_riverpod/features/number_trivia/domain/repositories/number_trivia_repository.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';

class NumberTriviaRepositoryImpl implements NumberTriviaRepository {
  NumberTriviaRepositoryImpl({
    required this.networkInfo,
    required this.remoteData,
    required this.localData,
  });

  final InternetConnectionChecker networkInfo;
  final NumberTriviaRemoteData remoteData;
  final NumberTriviaLocalData localData;

  @override
  Future<NumberTrivia> getConcreteNumberTrivia(int number) async {
    if (await networkInfo.hasConnection) {
      final remoteTrivia = await remoteData.getConcreteNumberTrivia(number);
      await localData.cacheNumberTrivia(remoteTrivia);
      return remoteTrivia;
    } else {
      final localTrivia = await localData.getLastNumberTrivia();
      return localTrivia;
    }
  }

  @override
  Future<NumberTrivia> getRandomNumberTrivia() async {
    if (await networkInfo.hasConnection) {
      final remoteTrivia = await remoteData.getRandomNumberTrivia();
      await localData.cacheNumberTrivia(remoteTrivia);
      return remoteTrivia;
    } else {
      final localTrivia = await localData.getLastNumberTrivia();
      return localTrivia;
    }
  }
}
