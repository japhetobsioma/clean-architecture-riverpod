import 'package:clean_architecture_riverpod/core/shared_preferences/shared_preferences_provider.dart';
import 'package:clean_architecture_riverpod/features/number_trivia/data/data_sources/local_data/number_trivia_local_data_impl.dart';
import 'package:clean_architecture_riverpod/features/number_trivia/data/data_sources/remote_data/number_trivia_remote_data_impl.dart';
import 'package:clean_architecture_riverpod/features/number_trivia/data/repositories/number_trivia_repository_impl.dart';
import 'package:clean_architecture_riverpod/features/number_trivia/domain/entites/number_trivia_entity.dart';
import 'package:clean_architecture_riverpod/features/number_trivia/domain/use_cases/number_trivia_use_cases.dart';
import 'package:clean_architecture_riverpod/features/number_trivia/presentation/providers/number_trivia_notifier.dart';
import 'package:http/http.dart' as http;
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:riverpod/riverpod.dart';

final numberTriviaProvider =
    StateNotifierProvider<NumberTriviaNotifier, AsyncValue<NumberTrivia>>(
  (ref) {
    final client = http.Client();
    final sharedPreferences = ref.watch(sharedPreferencesProvider);
    final remoteData = NumberTriviaRemoteDataImpl(client);
    final localData = NumberTriviaLocalDataImpl(sharedPreferences);
    final networkInfo = InternetConnectionChecker();
    final repository = NumberTriviaRepositoryImpl(
      remoteData: remoteData,
      localData: localData,
      networkInfo: networkInfo,
    );
    final useCase = NumberTriviaUseCases(repository);
    return NumberTriviaNotifier(useCase);
  },
  name: 'numberTriviaProvider',
);
