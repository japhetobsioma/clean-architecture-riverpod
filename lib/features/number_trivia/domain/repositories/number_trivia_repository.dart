import 'package:clean_architecture_riverpod/features/number_trivia/domain/entites/number_trivia_entity.dart';

abstract class NumberTriviaRepository {
  Future<NumberTrivia> getConcreteNumberTrivia(int number);

  Future<NumberTrivia> getRandomNumberTrivia();
}
