import 'dart:convert';

import 'package:clean_architecture_riverpod/core/exceptions/exceptions.dart';
import 'package:clean_architecture_riverpod/features/number_trivia/data/data_sources/local_data/number_trivia_local_data.dart';
import 'package:clean_architecture_riverpod/features/number_trivia/data/models/number_trivia_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NumberTriviaLocalDataImpl implements NumberTriviaLocalData {
  const NumberTriviaLocalDataImpl(this.sharedPreferences);

  final SharedPreferences sharedPreferences;

  static const cachedNumberTriviaKey = 'CACHED_NUMBER_TRIVIA';

  @override
  Future<NumberTriviaModel> getLastNumberTrivia() async {
    final jsonString = sharedPreferences.getString(cachedNumberTriviaKey);
    if (jsonString != null) {
      final jsonMap = json.decode(jsonString) as Map<String, dynamic>;
      return NumberTriviaModel.fromJson(jsonMap);
    } else {
      throw const CacheException();
    }
  }

  @override
  Future<void> cacheNumberTrivia(NumberTriviaModel numberTriviaModel) async {
    final jsonString = json.encode(numberTriviaModel.toJson());
    await sharedPreferences.setString(cachedNumberTriviaKey, jsonString);
  }
}
