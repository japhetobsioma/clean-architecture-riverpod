import 'package:clean_architecture_riverpod/features/number_trivia/domain/entites/number_trivia_entity.dart';
import 'package:clean_architecture_riverpod/features/number_trivia/presentation/providers/number_trivia_provider.dart';
import 'package:clean_architecture_riverpod/features/number_trivia/presentation/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class NumberTriviaPage extends StatelessWidget {
  const NumberTriviaPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Number Trivia'),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: Column(
              children: [
                const SizedBox(height: 10),
                Consumer(
                  builder: (context, watch, child) {
                    final numberTrivia = watch(numberTriviaProvider);
                    return numberTrivia.when(
                      data: (data) {
                        if (data == NumberTrivia.initial()) {
                          return const NumberTriviaInitialWidget();
                        } else {
                          return NumberTriviaDataWidget(data);
                        }
                      },
                      loading: () => const NumberTriviaLoadingWidget(),
                      error: (error, _) => NumberTriviaErrorWidget(error),
                    );
                  },
                ),
                const SizedBox(height: 20),
                const NumberTriviaFormWidget(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
