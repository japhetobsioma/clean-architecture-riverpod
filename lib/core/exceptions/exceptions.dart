class ServerException implements Exception {
  const ServerException();

  @override
  String toString() => 'Server Failure.';
}

class CacheException implements Exception {
  const CacheException();

  @override
  String toString() => 'Cache Failure.';
}
