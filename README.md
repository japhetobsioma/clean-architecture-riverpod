# Clean Architecture - Riverpod

Based on Resocoder's [Flutter TDD Clean Architecture](https://resocoder.com/flutter-clean-architecture-tdd/).


<br />

<h3 align="center">Architecture</h3>

<br />

<img src="./architecture-proposal.png" style="display: block; margin-left: auto; margin-right: auto; width: 75%;"/>

<br />
