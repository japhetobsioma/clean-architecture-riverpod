import 'package:clean_architecture_riverpod/features/number_trivia/domain/entites/number_trivia_entity.dart';
import 'package:clean_architecture_riverpod/features/number_trivia/domain/use_cases/number_trivia_use_cases.dart';
import 'package:riverpod/riverpod.dart';

class NumberTriviaNotifier extends StateNotifier<AsyncValue<NumberTrivia>> {
  NumberTriviaNotifier(
    this.useCase,
  ) : super(AsyncValue.data(NumberTrivia.initial()));

  final NumberTriviaUseCases useCase;

  Future<void> getConcreteNumberTrivia(String number) async {
    final intNumber = int.tryParse(number);
    const errorMessage =
        'Invalid Input - The number must be a positive integer or zero.';
    if (intNumber == null || intNumber.isNegative) {
      state = AsyncValue.error(Exception(errorMessage));
    } else {
      state = const AsyncValue.loading();
      state = await AsyncValue.guard(
        () => useCase.getConcreteNumberTrivia(intNumber),
      );
    }
  }

  Future<void> getRandomNumberTrivia() async {
    state = const AsyncValue.loading();
    state = await AsyncValue.guard(useCase.getRandomNumberTrivia);
  }
}
