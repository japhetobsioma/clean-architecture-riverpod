import 'package:flutter/material.dart';

class NumberTriviaInitialWidget extends StatelessWidget {
  const NumberTriviaInitialWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height / 3,
      child: const Center(
        child: SingleChildScrollView(
          child: Text(
            'Start searching',
            style: TextStyle(fontSize: 25),
            textAlign: TextAlign.center,
          ),
        ),
      ),
    );
  }
}
