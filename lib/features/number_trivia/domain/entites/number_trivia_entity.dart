import 'package:equatable/equatable.dart';

class NumberTrivia extends Equatable {
  const NumberTrivia({required this.text, required this.number});

  factory NumberTrivia.initial() {
    return const NumberTrivia(text: '', number: 0);
  }

  final String text;
  final int number;

  NumberTrivia copyWith({String? text, int? number}) {
    return NumberTrivia(text: text ?? this.text, number: number ?? this.number);
  }

  @override
  List<Object?> get props => [text, number];
}
