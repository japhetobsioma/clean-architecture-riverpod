import 'package:clean_architecture_riverpod/features/number_trivia/data/repositories/number_trivia_repository_impl.dart';
import 'package:clean_architecture_riverpod/features/number_trivia/domain/use_cases/get_concrete_number_trivia.dart';
import 'package:clean_architecture_riverpod/features/number_trivia/domain/use_cases/get_random_number_trivia.dart';
import 'package:equatable/equatable.dart';

class NumberTriviaUseCases extends Equatable {
  NumberTriviaUseCases(NumberTriviaRepositoryImpl repository)
      : getConcreteNumberTrivia = GetConcreteNumberTrivia(repository),
        getRandomNumberTrivia = GetRandomNumberTrivia(repository);

  final GetConcreteNumberTrivia getConcreteNumberTrivia;
  final GetRandomNumberTrivia getRandomNumberTrivia;

  @override
  List<Object?> get props => [getConcreteNumberTrivia, getRandomNumberTrivia];
}
