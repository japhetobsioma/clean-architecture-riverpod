import 'dart:developer';

import 'package:flutter_riverpod/flutter_riverpod.dart';

class AppProviderObserver extends ProviderObserver {
  const AppProviderObserver();

  @override
  void didAddProvider(ProviderBase provider, Object? value) {
    log('[didAddProvider] ${provider.name ?? provider.runtimeType}: $value');
  }

  @override
  void mayHaveChanged(ProviderBase provider) {
    log('[mayHaveChanged] ${provider.name ?? provider.runtimeType}');
  }

  @override
  void didUpdateProvider(ProviderBase provider, Object? newValue) {
    log(
      '[didUpdateProvider] ${provider.name ?? provider.runtimeType}: $newValue',
    );
  }

  @override
  void didDisposeProvider(ProviderBase provider) {
    log('[didDisposeProvider] ${provider.name ?? provider.runtimeType}');
  }
}
