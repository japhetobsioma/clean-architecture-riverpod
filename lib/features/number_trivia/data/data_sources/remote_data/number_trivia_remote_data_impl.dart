import 'dart:convert';

import 'package:clean_architecture_riverpod/core/exceptions/exceptions.dart';
import 'package:clean_architecture_riverpod/features/number_trivia/data/data_sources/remote_data/number_trivia_remote_data.dart';
import 'package:clean_architecture_riverpod/features/number_trivia/data/models/number_trivia_model.dart';
import 'package:http/http.dart' as http;

class NumberTriviaRemoteDataImpl implements NumberTriviaRemoteData {
  const NumberTriviaRemoteDataImpl(this.client);

  final http.Client client;

  static const baseUrl = 'numbersapi.com';
  static const header = {'Content-type': 'application/json'};

  @override
  Future<NumberTriviaModel> getConcreteNumberTrivia(int number) async {
    final request = Uri.http(baseUrl, '/$number');
    final response = await client.get(request, headers: header);
    if (response.statusCode == 200) {
      final responseJson = json.decode(response.body) as Map<String, dynamic>;
      return NumberTriviaModel.fromJson(responseJson);
    } else {
      throw const ServerException();
    }
  }

  @override
  Future<NumberTriviaModel> getRandomNumberTrivia() async {
    final request = Uri.http(baseUrl, '/random');
    final response = await client.get(request, headers: header);
    if (response.statusCode == 200) {
      final responseJson = json.decode(response.body) as Map<String, dynamic>;
      return NumberTriviaModel.fromJson(responseJson);
    } else {
      throw const ServerException();
    }
  }
}
