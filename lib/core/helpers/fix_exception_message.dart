/// Remove 'Exception:' string in the exception message.
String fixExceptionMessage(String error) {
  return error.replaceFirst('Exception:', '');
}
