import 'package:clean_architecture_riverpod/features/number_trivia/presentation/providers/number_trivia_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class NumberTriviaFormWidget extends HookWidget {
  const NumberTriviaFormWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final textController = useTextEditingController();
    final inputString = useState('');
    return Column(
      children: [
        TextField(
          controller: textController,
          keyboardType: TextInputType.number,
          decoration: const InputDecoration(
            border: OutlineInputBorder(),
            hintText: 'Input a number',
          ),
          onChanged: (value) => inputString.value = value,
          onSubmitted: (value) {
            inputString.value = value;
            textController.clear();
            context
                .read(numberTriviaProvider.notifier)
                .getConcreteNumberTrivia(inputString.value);
          },
        ),
        const SizedBox(height: 10),
        Row(
          children: [
            Expanded(
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: Theme.of(context).colorScheme.secondary,
                ),
                onPressed: () {
                  inputString.value = textController.text;
                  textController.clear();
                  context
                      .read(numberTriviaProvider.notifier)
                      .getConcreteNumberTrivia(inputString.value);
                },
                child: const Text('Search'),
              ),
            ),
            const SizedBox(width: 10),
            Expanded(
              child: ElevatedButton(
                onPressed: () {
                  textController.clear();
                  context
                      .read(numberTriviaProvider.notifier)
                      .getRandomNumberTrivia();
                },
                child: const Text('Get random trivia'),
              ),
            ),
          ],
        )
      ],
    );
  }
}
