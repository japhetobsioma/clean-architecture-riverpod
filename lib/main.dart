import 'package:clean_architecture_riverpod/core/app/app.dart';
import 'package:clean_architecture_riverpod/core/provider_observer/app_provider_observer.dart';
import 'package:clean_architecture_riverpod/core/shared_preferences/shared_preferences_provider.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  EquatableConfig.stringify = true;
  final sharedPreferences = await SharedPreferences.getInstance();
  runApp(
    ProviderScope(
      overrides: [
        sharedPreferencesProvider.overrideWithValue(sharedPreferences)
      ],
      observers: const [AppProviderObserver()],
      child: const App(),
    ),
  );
}
