import 'package:clean_architecture_riverpod/core/use_cases/use_case.dart';
import 'package:clean_architecture_riverpod/features/number_trivia/domain/entites/number_trivia_entity.dart';
import 'package:clean_architecture_riverpod/features/number_trivia/domain/repositories/number_trivia_repository.dart';

class GetConcreteNumberTrivia implements UseCase<NumberTrivia, int> {
  const GetConcreteNumberTrivia(this.repository);

  final NumberTriviaRepository repository;

  @override
  Future<NumberTrivia> call(int number) async {
    return repository.getConcreteNumberTrivia(number);
  }
}
