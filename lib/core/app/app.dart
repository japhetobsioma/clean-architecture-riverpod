import 'package:clean_architecture_riverpod/features/number_trivia/presentation/pages/number_trivia_page.dart';
import 'package:flutter/material.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Number Trivia',
      home: NumberTriviaPage(),
    );
  }
}
