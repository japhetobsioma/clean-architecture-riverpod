abstract class UseCase<R, T> {
  Future<R> call(T parameter);
}
